package main

import (
  "fmt"
  "net"
  "time"
  "log"
  "bytes"
  "net/http"
  "os"
  "regexp"
  "encoding/json"
  "io/ioutil"
  "strings"
  "flag" 
  "strconv"
)

func getRxTx(iface string) (int64, int64) {
  f, err := os.Open("/proc/net/dev")   
  if err != nil {
    panic(err)
  }
  body, err := ioutil.ReadAll(f)
  if err != nil {
    panic(err)
  }
  text := string(body)
  var rx, tx int64
  for _, line := range strings.Split(text, "\n")  {
    line = strings.Trim(line, " ")
    if strings.HasPrefix(line, iface) {
      line = strings.Split(line, ":")[1]
      matches := regexp.MustCompile("[0-9]+").FindAllStringSubmatch(line, -1)
      rx, err = strconv.ParseInt(matches[0][0], 10, 64)
      if err != nil {
        panic(err)
      }
      tx, err = strconv.ParseInt(matches[8][0], 10, 64)
      if err != nil {
        panic(err)
      }
      break
    }
  }
  fmt.Printf("rx=%d, tx=%d\n", rx, tx)
  return rx, tx
}

type Item struct {
  RX int64 `json:"rx"`
  TX int64 `json:"tx"`
  RXSpeed int64 `json:"rx_speed"` 
  TXSpeed int64 `json:"tx_speed"`
  HostName string `json:"hostname"`
  Timestamp int64 `json:"timestamp"`
  IP string `json:"ip"`
  Iface string `json:"iface"`
}

func PostTo(addr string, body []byte) { 
  log.Printf("post to: %s, data=%s\n", addr, body)
  r := bytes.NewBuffer(body)

    response, err := http.Post(addr, "binary/octet-stream", r)

    if err != nil {
      log.Printf("faild to send data to server: %s\n", err.Error())
      return
    }
    defer response.Body.Close() 
    content, err := ioutil.ReadAll(response.Body) 
    if err != nil {
        log.Fatal(err)
    } 
    if response.StatusCode != 200 {
      log.Printf("bad status: %d, %s\n",response.StatusCode, content)
    } 
}


func findLocalIp(ifaceName string) (string, string) {
	ifaces, err := net.Interfaces()
	if err != nil {
		panic(err)
	}
	for _, iface := range ifaces {
		addrs, err := iface.Addrs()
		if err != nil {
			continue
		}
		for _, addr := range addrs { 
      if len(ifaceName) != 0 && ifaceName == addr.String() { 
				return ifaceName, strings.Split(addr.String(), "/")[0]
      } else { 
        if strings.HasPrefix(addr.String(), "192.168") || 
          strings.HasPrefix(addr.String(), "172.") || 
          strings.HasPrefix(addr.String(), "10.") {
          fmt.Printf("iface %s, ip %s\n", iface.Name, addr.String())
          return iface.Name, strings.Split(addr.String(), "/")[0]
        }
		}
    }

  }
	return "lo", "127.0.0.1"
}

func reportLoop(iface string, interval int64, server string) { 
  iface,  ip := findLocalIp(iface) 
  for {
    rx, tx := getRxTx(iface) 
    time.Sleep(time.Duration(interval) * time.Second)
    rx2, tx2 := getRxTx(iface)
    avgRx := (rx2 - rx) / int64(interval)
    avgTx := (tx2 - tx) / int64(interval)
    log.Printf("receive: %d/s KB, send: %d/s KB\n", avgRx/1024, avgTx/1024)
    name, err := os.Hostname()
    if err != nil {
      panic(err)
    }
    item := Item {
      RX: rx2,
      TX: tx2,
      RXSpeed: avgRx/1024,
      TXSpeed: avgTx/1024,
      HostName: name,
      Timestamp: time.Now().Unix(), 
      IP: ip,
      Iface: iface,
    }
    body, err := json.Marshal(item)
    if err != nil {
      panic(err)
    }
    PostTo(server, body)
  }
}

var clientMap map[string]Item

func serverLoop(server string) { 
  http.HandleFunc("/", func(w http.ResponseWriter, r *http.Request) {
    defer r.Body.Close()
    body, err := ioutil.ReadAll(r.Body)
    if err != nil {
      log.Printf("read body from client failed: %s\n", err.Error())
      return
    }
    var item Item
    err = json.Unmarshal(body, &item)
    if err != nil {
      log.Printf("unable to unmarshal json: %s\n", err.Error())
      return
    } 
    log.Printf("collect data from ip: %s %s\n", item.IP, body)
    clientMap[item.IP] =item 
  }) 
  log.Fatal(http.ListenAndServe(server, nil))
}

func main() {
  var iface string
  var mode string
  var interval int64
  var server string
  flag.StringVar(&iface, "iface", "",  "interface name")
  flag.Int64Var(&interval, "interval", 10, "speed test interval")
  flag.StringVar(&server, "server", "", "server address, for client http://ip:port,  for server :8080")
  flag.StringVar(&mode, "mode", "client", "client or server")
  flag.Parse()
  clientMap = make(map[string]Item)

  if mode == "client" {
    reportLoop(iface, interval, server)
  } else if mode == "server" {
    serverLoop(server) 
  } 
}
